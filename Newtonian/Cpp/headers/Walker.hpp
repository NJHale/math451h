#ifndef WALKER_H
#define WALKER_H

#include <cstdlib>
#include <math.h>
#include "ImportantTypes.hpp"

/*
 Perform a random walk beginning at position p, and return the position at which 
 the walk makes contact with the aggregate.
 */
Position walk(int nelm, bool **space, double R, double A, double B, double l);
Position walk(int nelm, bool **space, double R, Position p);
Position outside_walk(int nelm, double R, Position pos);

#endif