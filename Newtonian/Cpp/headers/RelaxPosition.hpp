#ifndef RELAXPOSITION_H
#define RELAXPOSITION_H

#include <cstdlib>
#include <stdio.h>
#include "ImportantTypes.hpp"

int countNeighbors(int nelm, bool **space, Position p);

Position relaxPosition(int nelm, bool **space, Position p);


#endif