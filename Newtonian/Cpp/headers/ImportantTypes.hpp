#ifndef IMPORTANTTYPES_H
#define IMPORTANTTYPES_H

#define PI 3.14159265359

enum Direction {
    right,
    up,
    left,
    down
};

struct Position {
    int x;
    int y;
};

#endif