#ifndef FINDPROBABILITY_H
#define FINDPROBABILITY_H

#include "ImportantTypes.hpp"

double findProbability(int nelm, bool **space, double A, double B, double l, Position p);

#endif