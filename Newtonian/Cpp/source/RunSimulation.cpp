#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../headers/FractalDimension.hpp"
#include "../headers/Walker.hpp"
#include "../headers/HolePrevent.hpp"
#include "../headers/ImportantTypes.hpp"
/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
 and may not be redistributed without written permission.*/
/*
//Using SDL and standard IO
#include <SDL2/SDL.h>

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main( int argc, char* args[] )
{
    //The window we'll be rendering to
    SDL_Window* window = NULL;
    
    //The surface contained by the window
    SDL_Surface* screenSurface = NULL;
    
    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    }
    else
    {
        //Create window
        window = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( window == NULL )
        {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
        }
        else
        {
            //Get window surface
            screenSurface = SDL_GetWindowSurface( window );
            
            //Fill the surface white
            SDL_FillRect( screenSurface, NULL, SDL_MapRGB( screenSurface->format, 0xFF, 0xFF, 0xFF ) );
            
            //Update the surface
            SDL_UpdateWindowSurface( window );
            
            //Wait two seconds
            SDL_Delay( 2000 );
        }
    }
    
    //Destroy window
    SDL_DestroyWindow( window );
    
    //Quit SDL subsystems
    SDL_Quit();
    
    return 0;
}
*/
void printSpace(int nelm, bool **space) {
    printf("\n  ");
    for (int i = 0; i < nelm; i++) {
        //printf("%.2i", i);
    }
    printf("\n");
    for (int i = 0; i < nelm; i++) {
        //printf("%.2i", i);
        for (int j = 0; j < nelm; j++) {
            printf("%c", space[i][j] ? '.' : '#');
        }
        printf("\n");
    }
}

int run(int nelm, int nwalkers, double A, double B, double l) {
    //even numbers only. powers of 2 preferred
    nelm = nelm & ~1;
    
    //make an nxn array to hold stuck walkers.
    bool** space = 0;
    space = new bool*[nelm];
    for (int i = 0; i < nelm; i++) {
        space[i] = new bool[nelm];
        for (int j = 0; j < nelm; j++) {
            //make sure to set all of the elements to 0 initially
            space[i][j] = 0;
        }
    }
    
    //stick a 2x2 block of walkers in the center of the thing.
    space[nelm/2][nelm/2] = 1;
    space[nelm/2-1][nelm/2] = 1;
    space[nelm/2][nelm/2-1] = 1;
    space[nelm/2-1][nelm/2-1] = 1;
    
    printSpace(nelm, space);
    
    //the radius of the circle upon which walkers will spawn
    int R = 5;
    
    //perform the walks
    for (int i = 0; i < nwalkers; i++) {
        if (i%1000==0)
            printf("%i\n", i);
        
        //perform a preliminary walk
        Position pos = walk(nelm, space, R, A, B, l);
        
        //conclude walk by killing the walker in its current position
        space[pos.y][pos.x] = 1;
        
        //if necessary, increase the radius of the circle upon which
        if ((pos.x-nelm/2)*(pos.x-nelm/2) + (pos.y-nelm/2)*(pos.y-nelm/2) > (R-4)*(R-4)) {
            R++;
        }
    }
    
    printSpace(nelm, space);
    
    
    //delete the space array
    for (int i = 0; i < nelm; i++) {
        delete[] space[i];
    }
    delete[] space;
    return 0;
}

int main() {
    run(70, 500, 1, 0.5, 9);
    return 0;
}