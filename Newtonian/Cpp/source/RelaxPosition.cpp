#include "../headers/RelaxPosition.hpp"


int countNeighbors(int nelm, bool **space, Position p, int dist) {
    int count = 0;
    for (int i = -dist; i <= dist; i++) {
        for (int j = -dist; j <= dist; j++) {
            if (p.x+i >= 0 && p.x+i < nelm && p.y+j >= 0 && p.y+j < nelm) {
                if (space[p.y+j][p.x+i]) {
                    count++;
                }
            }
        }
    }
    return count;
}


Position relaxPosition(int nelm, bool **space, Position p, int dist) {
    
    int max = countNeighbors(nelm, space, p, 1);
    Position pmax = p;
    int n_tied = 1;
    Position piter;
    
    for (piter.x = p.x - dist; piter.x <= p.x + dist; piter.x++) {
        for (piter.y = p.y - dist; piter.y <= p.y + dist; piter.y++) {
            if ((piter.x != p.x || piter.y != p.y) && !space[piter.y][piter.x]) {
                int n_neighbors = countNeighbors(nelm, space, piter, 1);
                if (n_neighbors > max) {
                    max = n_neighbors;
                    pmax = piter;
                    n_tied = 1;
                } else if (max > 0 && n_neighbors == max) {
                    n_tied++;
                    if (((double) rand() / (RAND_MAX)) < 1/((double)n_tied)) {
                        pmax = piter;
                    }
                }
            }
        }
    }
    return pmax;
}

Position relaxPosition(int nelm, bool** space, Position p) {
    return relaxPosition(nelm, space, p, 1);
}