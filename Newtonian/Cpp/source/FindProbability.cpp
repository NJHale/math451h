#include "../headers/FindProbability.hpp"
#include <stdio.h>

double findProbability(int nelm, bool **space, double A, double B, double l, Position p) {
    int n = 0;
    for (int i = p.y - 4; i <= p.y + 4; i++) {
        for (int j = p.x - 4; j <= p.x + 4; j++) {
            if (space[i][j]) {
                n++;
            }
        }
    }
    
    double probability = A*((((double)n)/(l*l))-((l-1)/(2*l)))+ B;
    
    return probability > 0 ? probability : 0;
}