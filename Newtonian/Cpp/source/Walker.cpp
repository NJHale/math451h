#include "../headers/Walker.hpp"
#include "../headers/RelaxPosition.hpp"
#include "../headers/FindProbability.hpp"
#include <stdio.h>

#define PROB_REENTER 0.3

Position outside_walk(int nelm, double R, Position pos) {
    double angle = atan2(pos.x - nelm/2, pos.y - nelm/2);
    double original = angle;
    double rn;
    
    while (true) {
        rn = ((double) rand() / (RAND_MAX));
        
        if (rn < PROB_REENTER) {
            pos.x = nelm/2 + (R-1) * cos(angle);
            pos.y = nelm/2 + (R-1) * sin(angle);
            return pos;
        } else if (rn < 1-(1-PROB_REENTER)/2) {
            angle += 1/R;
        } else {
            angle -= 1/R;
        }
    }
}

Position walk(int nelm, bool **space, double R, Position pos) {
    Position dup = pos;
    double half_nelm = nelm/2;
    while (true) {
        switch(static_cast<Direction>(rand() % 4)) {
            case left :
                if (space[pos.y][pos.x-1])
                    return pos;
                pos.x--;
                break;
            case right :
                if (space[pos.y][pos.x+1])
                    return pos;
                pos.x++;
                break;
            case up :
                if (space[pos.y-1][pos.x])
                    return pos;
                pos.y--;
                break;
            case down :
                if (space[pos.y+1][pos.x])
                    return pos;
                pos.y++;
                break;
        }
        
        if (space[pos.y][pos.x-1] || space[pos.y][pos.x+1] || space[pos.y-1][pos.x] || space[pos.y+1][pos.x]) {
            return pos;
        }
        
        if ((pos.x-half_nelm)*(pos.x-half_nelm) + (pos.y-half_nelm)*(pos.y-half_nelm) > R*R) {
            pos = outside_walk(nelm, R, pos);
        }
    }
}

Position walk(int nelm, bool **space, double R, double A, double B, double l) {
    double theta = 2.0 * PI * ((double) rand() / (RAND_MAX));
    Position pos;
    pos.x = nelm/2 + (R-1) * cos(theta);
    pos.y = nelm/2 + (R-1) * sin(theta);
    
    do {
        pos = walk(nelm, space, R, pos);
    } while (((double) rand() / (RAND_MAX)) < findProbability(nelm, space, A, B, l, pos));
    
    Position prev;
    do {
        prev = pos;
        pos = relaxPosition(nelm, space, pos);
    } while (prev.x != pos.x || prev.y != pos.y);
    
    return pos;
}